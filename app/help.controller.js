(function () {
    'use strict';
    angular
        .module('app')
        .controller('HelpController', HelpController);

    HelpController.$inject = ['PublicService'];

    function HelpController(PublicService) {

        var self = this;

        self.videoList = [];
        self.selectedStep = {};

        init();

        self.selectStep = selectStep;
        self.close = close;

        function close(video) {
            video.player.stopVideo();
            self.selectedStep = {};
        }

        function selectStep(step) {
            self.selectedStep = step;
        }

        function init() {
            return PublicService.getActiveRally()
                .then(function (result) {
                    if (result && result.length > 0) {
                        self.videoList = result[0].resources;
                        self.playerVars = {
                            controls: 1,
                            autoplay: 0
                        };
                    }
                });
        }
    }

})();