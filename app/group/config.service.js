(function() {
    'use strict';
    angular
        .module('app')
        .factory('ConfigService', ConfigService);

    ConfigService.$inject = ['$http'];

    function ConfigService($http) {
        var SERVER = '/rm-server-web/rs/';
        return {
            createAccount: createAccount
        }


        function createAccount(data) {
            return $http.post(SERVER + '/groups', data)
                .then(getResponseOK)
                .catch(getResponseFailed);
            function getResponseOK(response) {
                return response.data;
            }
            function getResponseFailed(error) {
                console.log('XHR Failed for create account.' + error.data);
            }
        }




    }


})();