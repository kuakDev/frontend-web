(function() {
    'use strict';

    angular
        .module('app')
        .controller('AvatarController', AvatarController);

    AvatarController.$inject = ['$location', 'CatalogService', 'ConfigFactory'];

    function AvatarController($location, CatalogService, ConfigFactory) {
        var self = this;


        self.avatars = [];
        self.activeAvatars = {};
        
        self.selectedAvatar = {};
        
        self.redirectURL = "transport";

        self.saveAvatar = function() {
            ConfigFactory.setAvatar(self.selectedAvatar.id);
            $location.path(self.redirectURL);
        };
        
        self.selectAvatar = function(avatar) {
           self.selectedAvatar = avatar;
        };
        

        loadAvatars();

        function loadAvatars() {
            return CatalogService.getListAvatars()
                .then(function(response) {
                    if (response) {
                        for (var start = 0; start < response.length; start += 4) {
                            var end = Math.min(start + 4, response.length);
                            var listAvatars = response.slice(start, end);
                            if (start == 0) {
                                self.activeAvatars = listAvatars;
                            } else {
                                self.avatars.push(listAvatars);
                            }
                        }
                        self.selectedAvatar = self.activeAvatars[0];
                    } else {

                    }
                });
        }

    }


})();