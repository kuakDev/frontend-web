(function() {
    'use strict';

    angular
        .module('app')
        .controller('ConfigController', ConfigController);

    ConfigController.$inject = ['$location', 'CatalogService', 'ConfigFactory'];

    function ConfigController($location, CatalogService, ConfigFactory) {
        var self = this;

        var msgError= "";

        self.groups_="";

        self.group = {};
        self.group.members = [];

        self.countries = [];
        self.levels = [];
        self.avatars = [];
        self.transports = [];
        self.institutions = [];
        
        self.activeAvatars = {};
        
        var redirectURL = "avatar";
        
        
        self.saveGroup = function() {
            
            ConfigFactory.setGroup(self.group);
            
            if (isFormValid()){
                $location.path(redirectURL);
            }else{
                swal({
                    title: "Verifique",
                    text: msgError,
                    type: "warning",
                    confirmButtonColor: "#AEEA00",
                    confirmButtonText: "Continuar",
                    closeOnConfirm: true
                },function() {
                    ;
                });
            }
            
        };

        self.isFormValid = function(){
           if (!isFormValid()){
                swal({
                    title: "Verifique",
                    text: msgError,
                    type: "warning",
                    confirmButtonColor: "#AEEA00",
                    confirmButtonText: "Continuar",
                    closeOnConfirm: true
                },function() {
                    ;
                });
            }
        }
        
        loadCatalogCountries();

        function isNull(valor){
            if(valor !== null && valor !== undefined && valor !== "NULL"
                && valor !== "null" && valor !== "undefined"  && valor !== ""){
                return false;
            }else {
                return true;
            }
        }
        
        function isFormValid(){
            
            msgError="";
            

            if (isNull(self.group.user)){
                msgError="Ingrese un nombre de equipo \n";
                self.group.user = '';
                $("[name='inputUser']").focus();
                return false;
            }else if (!isUnique(self.group.user)){
                msgError="Este nombre de equipo ya está en uso, elige otro\n";
                $("[name='inputUser']").focus();
                return false;
            }else if (isNull(self.group.institution.name)) {
                msgError="Ingrese el nombre de institución educativa\n";
                $("[name='inputInstitution']").focus();
                return false;
            }else if ((self.group.password != self.group.password2 )|| 
                        isNull(self.group.password)||isNull(self.group.password2)){
                msgError="Las contraseñas no coinciden \n";
                self.group.password = '';
                self.group.password2 = '';
                $("[name='inputPassword']").focus();
                return false;    
            }else if (isNull(self.group.email)){
                msgError="Ingrese su correo electrónico \n";
                $("[name='inputEmail']").focus();
                return false;   
            }else if (isNull(self.group.country)){
                msgError="Seleccione un país \n";
                $("[name='inputCountry']").focus();
                return false;   
            }else if (isNull(self.group.level)){
                msgError="Seleccione un nivel\n";
                $("[name='inputLevel']").focus();
                return false;   
            }else{
                return true;
            }
            
        }

        function loadCatalogCountries() {
            return CatalogService.getListCountries()
                .then(function(result) {
                    self.countries = result;
                });
        }


        loadRallyLevel();

        function loadRallyLevel() {
            return CatalogService.getListLevels()
                .then(function(result) {
                    self.levels = result;
                });
        }

        loadInstitutions();

        function loadInstitutions() {
            return CatalogService.getListInstitutions()
                .then(function(result) {
                    self.institutions = result;
                });
        }


        loadTransports();

        function loadTransports() {
            return CatalogService.getListTransports()
                .then(function(result) {
                    self.transports = result;
                });
        }

        loadGroup();

        function loadGroup() {
            return CatalogService.getListGroups()
                .then(function(result) {
                    self.groups_ = result;
                });
        }

        function isUnique(user){
            var isUnique_ = true;
            for (var i in self.groups_) {
                if(self.groups_[i].user.toUpperCase().trim() ==  user.toUpperCase().trim()){
                    isUnique_ = false;
                }
            }

            return isUnique_;
        }


        $("[name='inputUser']").focus();
    }


})();