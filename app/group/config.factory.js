(function() {
    'use strict';

    angular
        .module('app')
        .factory('ConfigFactory', ConfigFactory);

    ConfigFactory.$inject = [];

    function ConfigFactory() {
        var group = {};


        return {
            setGroup: setGroup,
            setAvatar: setAvatar,
            setTransport: setTransport,
            getGroup: getGroup
        }

        function setGroup(data) {
            group = data;
        }

        function setAvatar(id) {
            if (!group.avatar) {
                group.avatar = {};
            }
            group.avatar = { "id": id };
        }

        function setTransport(id) {
            if (!group.transport) {
                group.transport = {};
            }
            group.transport = { "id": id };
        }

        function getGroup() {
            return group;
        }
    }

})();