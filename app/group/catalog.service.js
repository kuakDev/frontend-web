(function() {
    'use strict';
    angular
        .module('app')
        .factory('CatalogService', CatalogService);

    CatalogService.$inject = ['$http'];

    function CatalogService($http) {
        var SERVER = '/rm-server-web/rs/';
        return {
            getListCountries: getListCountries,
            getListAvatars: getListAvatars,
            getListTransports: getListTransports,
            getListLevels: getListLevels,
            getListInstitutions: getListInstitutions,
            getListGroups: getListGroups
        }

        function getListCountries() {
            return $http.get(SERVER + 'catalog/countries')
                .then(getResponseOk)
                .catch(getResponseFailed);

            function getResponseOk(response) {
                return response.data;
            }

            function getResponseFailed(error) {
                console.log('XHR Failed for servide get Countries');
            }
        }

        function getListAvatars() {
            return $http.get(SERVER + 'catalog/assets/avatars')
                .then(getResponseOK)
                .catch(getResponseFailed);
            function getResponseOK(response) {
                return response.data;
            }
            function getResponseFailed(error) {
                console.log('XHR Failed for get avatars.' + error.data);
            }
        }

        function getListTransports() {
            return $http.get(SERVER + 'catalog/assets/transports')
                .then(getResponseOK)
                .catch(getResponseFailed);
            function getResponseOK(response) {
                return response.data;
            }
            function getResponseFailed(error) {
                console.log('XHR Failed for get transports.' + error.data);
            }
        }

        function getListLevels() {
            return $http.get(SERVER + 'catalog/levels')
                .then(getResponseOk)
                .catch(getResponseFailed);

            function getResponseOk(response) {
                return response.data;
            }

            function getResponseFailed(error) {
                console.log('XHR Failed for servide get Levels');
            }
        }
        
        function getListInstitutions() {
            return $http.get(SERVER + 'catalog/institutions')
                .then(getResponseOk)
                .catch(getResponseFailed);

            function getResponseOk(response) {
                return response.data;
            }

            function getResponseFailed(error) {
                console.log('XHR Failed for servide get Institutions');
            }
        }

        function getListGroups() {
            return $http.get(SERVER + 'groups')
                .then(getResponseOk)
                .catch(getResponseFailed);

            function getResponseOk(response) {
                return response.data;
            }

            function getResponseFailed(error) {
                console.log('XHR Failed for servide get Groups');
            }
        }




    }


})();