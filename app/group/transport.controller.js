(function() {
    'use strict';

    angular
        .module('app')
        .controller('TransportController', TransportController);

    TransportController.$inject = ['$location', 'CatalogService', 'ConfigFactory', 'ConfigService'];

    function TransportController($location, CatalogService, ConfigFactory, ConfigService) {
        var self = this;


        self.transport = [];
        self.activeTransports = {};

        self.selectedTransport = {};

        self.redirectURL = "login";

        self.saveTransport = function() {
            ConfigFactory.setTransport(self.selectedTransport.id);
            return ConfigService.createAccount(ConfigFactory.getGroup())
                .then(function(result) {
                    if (result && result['@type']) {
                        swal({
                            title: "¡Bien Hecho!",
                            text: "Ahora debes iniciar sesion.",
                            imageUrl: "/assets/images/common/bien-hecho.png",
                            type: "success",
                            confirmButtonColor: "#AEEA00",
                            confirmButtonText: "Continuar",
                            closeOnConfirm: true
                        }, function() {
                             window.location.href = "/index.html#/login";
                        });
                    } else {
                        swal({
                            title: "¡Ohhh! Ha ocurrido un error.",
                            text: "Por favor registrate nuevamente.",
                            type: "warning",
                            confirmButtonColor: "#AEEA00",
                            confirmButtonText: "Continuar",
                            closeOnConfirm: true
                        }, function() {
                            window.location.href = "/index.html#/signup";
                        });
                    }
                    
                    
                });
        };

        self.selectTransport = function(transport) {
            self.selectedTransport = transport;
        };


        loadTransports();

        function loadTransports() {
            return CatalogService.getListTransports()
                .then(function(response) {
                    if (response) {
                        for (var start = 0; start < response.length; start += 4) {
                            var end = Math.min(start + 4, response.length);
                            var listTransports = response.slice(start, end);
                            if (start == 0) {
                                self.activeTransports = listTransports;
                            } else {
                                self.transport.push(listTransports);
                            }
                        }
                        self.selectedTransport = self.activeTransports[0];
                    } else {

                    }
                });
        }

    }


})();