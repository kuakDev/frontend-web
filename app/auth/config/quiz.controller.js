(function () {
    'use strict';
    angular
        .module('app.auth')
        .controller('QuizController', QuizController);

        QuizController.$inject = ['PrivateFactory', 'InitializerFactory', 'PrivateService'];




    function QuizController(PrivateFactory, InitializerFactory, PrivateService) {

        var self = this;

        InitializerFactory.setInit(init);

        PrivateFactory.getDataGroup();

        function init() {


            self.data = PrivateFactory.data;
            
            self.showSiguiente = false;


            self.data.quiz =  [
                {id: 31, id_: 1,answer:'', text:'Mi clase de matemáticas es muy alegre', image:'', 
                posibleAnswers: [{id:'a', value: 'Sí'},{id:'b', value: 'A veces'},{id:'c', value: 'Nunca'}]},
                {id: 32, id_: 2,answer:'', text:'En mi clase uso materiales como loterías, pelotas, monedas, cubos, dados o tangram para aprender matemáticas', image:'', 
                posibleAnswers: [{id:'a', value: 'Sí'},{id:'b', value: 'A veces'},{id:'c', value: 'Nunca'}]},
                {id: 33, id_: 3,answer:'', text:'Trabajo con mis amigos en la clase de matemáticas', image:'', 
                posibleAnswers: [{id:'a', value: 'Sí'},{id:'b', value: 'A veces'},{id:'c', value: 'Nunca'}]},
                {id: 34, id_: 4,answer:'', text:'Creo que soy muy bueno en matemáticas', image:'', 
                posibleAnswers: [{id:'a', value: 'Sí'},{id:'b', value: 'A veces'},{id:'c', value: 'Nunca'}]},
                {id: 35, id_: 5,answer:'', text:'Me gustaría jugar en la clase de matemáticas. ', image:'', 
                posibleAnswers: [{id:'a', value: 'Sí'},{id:'b', value: 'A veces'},{id:'c', value: 'Nunca'}]},
                {id: 36, id_: 6,answer:'', text:'Te ha gustado el rally deja tus comentarios', image:'', 
                posibleAnswers: []}

                
            ];

              //console.log(self.data.group.registrations[0].id);

        }

        self.validateAnswers = function (answer, id) {
            //console.log(self.data.quiz)
        
            var bandera=true;
            
            self.data.quiz.forEach(function(q) {
                if (q.id == id && id !=36)
                    q.answer = answer.id;
                else if (q.id == id )
                    q.answer = answer
                
                if (q.answer == "")
                    bandera=false;
                    

            });

            if (bandera)
                self.showSiguiente = true;
            
        }
        
       
        
        self.sendAnswers = function (){

            var registration = self.data.group.registrations[0].id;
            var quiz = self.data.quiz;
            var type = 2;

            //console.log(JSON.stringify({type: type, registration: registration, quiz:quiz}));

            $.ajax({
                type: "POST",
                url: "/rm-server-quiz/answers",
                contentType: "application/json",
                data: JSON.stringify({type: type, registration: registration, quiz:quiz}),
                dataType: "text", 
                success: function (data) { //alert(data);
                    
                    
                    window.location.href = "/auth/welcome.html#/";
              
        
                },
                failure: function (errMsg) {
                    //alert(errMsg);
                }
            });

        }

    }


})();