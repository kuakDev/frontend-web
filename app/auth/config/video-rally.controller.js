(function () {
    'use strict';
    angular
        .module('app.auth')
        .controller('VideoRallyController', VideoRallyController);

    VideoRallyController.$inject = ['PrivateFactory', 'PrivateService', '$scope', 'InitializerFactory'];

    function VideoRallyController(PrivateFactory, PrivateService, $scope, InitializerFactory) {
        var self = this;

        self.videoList = [];


        InitializerFactory.setInit(init);

        PrivateFactory.getDataGroup();

        self.selectStep = selectStep;
        self.close = close;

        function close() {
            self.selectedStep = {};
        }

        function selectStep(step) {
            self.selectedStep = step;
        }

        function init() {
            self.data = PrivateFactory.data;
            
            var registredCountries = self.data.group.registrations[0].registrationCountries;
            var rallyCountries = self.data.group.registrations[0].rally.rallyCountries;
            var countriesToShow = registredCountries.length > 0 ? registredCountries : rallyCountries;
            
            angular.forEach(countriesToShow, function (countries) {
                angular.forEach(countries.country.resources, function (resource) {
                    if (resource.type === 'video') {
                        self.videoList.push(resource);
                    }
                });
            });
        }

        self.logout = function () {
            return PrivateService.logoff()
                .then(function () {
                    window.location.href = "/index.html";
                });
        };


    }

})();