(function () {
    'use strict';
    angular
        .module('app.auth')
        .controller('GasController', GasController);

    GasController.$inject = ['PrivateFactory', 'PrivateService', 'InitializerFactory'];

    function GasController(PrivateFactory, PrivateService, InitializerFactory) {

        var gas = new Array('/assets/images/gas/gas0.png',
            '/assets/images/gas/gas1.png',
            '/assets/images/gas/gas2.png',
            '/assets/images/gas/gas3.png',
            '/assets/images/gas/gas4.png',
            '/assets/images/gas/gas5.png');

        var contGas = 1;



        var self = this;
        InitializerFactory.setInit(init);

        PrivateFactory.getDataGroup();

        self.multipleValueQuestions = [];

        self.scoreRombos = 20;

        self.scoreGas = "0";

        self.validateAnswers = function (answer, id) {

            if (answer.correct) {
                self.scoreGas = 100;

                if (contGas <= 5) {
                    //console.log("contGas:" + contGas);
                    self.imgUrl = gas[contGas++];

                    self.imgClass = 'animated wobble';

                    setTimeout(function () {
                        self.imgClass = '';
                    }, 1000);

                    $("[name=" + id + "]").attr('disabled', 'disabled');

                    if (contGas == 6) {
                        swal({
                            title: "¡Buen trabajo!",
                            text: "Haz llenado el tanque, selecciona continuar",
                            type: "success",
                            confirmButtonColor: "#AEEA00",
                            confirmButtonText: "Entendido",
                            closeOnConfirm: true,
                            allowEscapeKey: false
                        }, function () {
                                var answer = {
                                    "points": self.scoreRombos,
                                    "resources": []
                                };
                
                            return PrivateService.saveScoreMultipleValueQuestion(id, answer)
                            .then(function(result){
                                self.showSiguiente = true;
                            });
                            
                            
                        });

                    } else {

                        swal({
                            title: "¡Tu respuesta es correcta!",
                            text: "Continúa así.",
                            type: "info",
                            timer: 2000,
                            showConfirmButton: false
                        });



                    }



                }

            } else {
                if (self.scoreRombos >= 1) {

                    $("[id=" + answer.id + "]").attr('disabled', 'disabled');

                    self.scoreRombos = self.scoreRombos - 1;
                    self.romboClass = 'animated fadeInUp';
                    self.numberClass = 'animated tada';

                    setTimeout(function () {
                        self.romboClass = '';
                        self.numberClass = '';
                    }, 10);


                    swal({
                        title: "¡Te has equivocado, intenta nuevamente!",
                        text: "",
                        type: "error",
                        timer: 2000,
                        showConfirmButton: false
                    });



                }


            }


        }



        function init() {


            self.imgUrl = '/assets/images/gas/gas0.png';
            self.romboUrl = '/assets/images/welcome/rombo.png';

            self.showSiguiente = false;

            self.data = PrivateFactory.data;
            if (self.data.group.registrations[0].registrationCountries.length > 0) {
                angular.forEach(self.data.group.registrations[0].registrationCountries, function (countries) {
                    if (countries.country.id === self.data.workingCountry) {
                        if (self.data.rally && self.data.rally.id) {
                            return PrivateService.getMutipleValueQuestions(self.data.rally.id, countries.country.id)
                                .then(function (result) {
                                    if (result) {
                                        self.multipleValueQuestions = result;
                                    }
                                });
                        }
                    }
                });
            } else {
                swal({
                    title: "!Importante!",
                    text: "En este momento no encontramos el recurso para cargar gasolina.",
                    type: "error",
                    confirmButtonColor: "#AEEA00",
                    confirmButtonText: "Entendido",
                    closeOnConfirm: true
                }, function () {
                    window.location.href = '#map-countries';
                });
            }




        }




    }

})();