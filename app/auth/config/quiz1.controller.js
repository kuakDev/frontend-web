(function () {
    'use strict';
    angular
        .module('app.auth')
        .controller('Quiz1Controller', Quiz1Controller);

        Quiz1Controller.$inject = ['PrivateFactory', 'InitializerFactory', 'PrivateService'];

    function Quiz1Controller(PrivateFactory, InitializerFactory, PrivateService) {
        var self = this;

        InitializerFactory.setInit(init);

        PrivateFactory.getDataGroup();

        function init() {
            self.data = PrivateFactory.data;
            
            self.showSiguiente = false;

            self.data.quiz =  [
                {id: 1,id_: 1 ,answer:'', text:'¿Qué forma tienen las pelotas?', image:'/assets/images/quiz/level1/q1.png', 
                posibleAnswers: [{id:'a', value: 'Círculos'},{id:'b', value: 'Cubos'},{id:'c', value: 'Esferas'}]},
                {id: 2,id_: 2 ,answer:'', text:'¿Qué nombre recibe el ángulo verde del dibujo? ', image:'/assets/images/quiz/level1/q2.png', 
                posibleAnswers: [{id:'a', value: 'Agudo'},{id:'b', value: 'Recto'},{id:'c', value: 'Obtuso'}]},
                {id: 3,id_: 3 ,answer:'', text:'¿Cuáles figuras continúan correctamente el patrón?', image:'/assets/images/quiz/level1/q3.png', 
                posibleAnswers: [{id:'a', value: 'a'},{id:'b', value: 'b'},{id:'c', value: 'c'}]},
                {id: 4,id_: 4 ,answer:'', text:'Cuál de las figuras no tiene eje de simetría?', image:'/assets/images/quiz/level1/q4.png', 
                posibleAnswers: [{id:'a', value: 'a'},{id:'b', value: 'b'},{id:'c', value: 'c'}]},
                {id: 5,id_: 5 ,answer:'', text:'Así se escribe “mitad” como fracción:', image:'', 
                posibleAnswers: [{id:'a', value: '1/1'},{id:'b', value: '2/1'},{id:'c', value: '1/2'}]},
                {id: 6,id_: 6 ,answer:'', text:'¿Cuántos helados de limón se vendieron?', image:'/assets/images/quiz/level1/q6.png', 
                posibleAnswers: [{id:'a', value: '3 helados'},{id:'b', value: '5 helados'},{id:'c', value: '15 helados'}]},
                {id: 7,id_: 7 ,answer:'', text:'¿Cuál de las siguientes medidas es mayor que las demás? ', image:'', 
                posibleAnswers: [{id:'a', value: '1 centena'},{id:'b', value: '3 decenas y 2 unidades'},{id:'c', value: '4 unidades y 1 decena'}]},
                {id: 8,id_: 8 ,answer:'', text:'¿Qué sigue en este patrón 45, 47, 49…?', image:'', 
                    posibleAnswers: [{id:'a', value: '50'},{id:'b', value: '52'},{id:'c', value: '51'}]},
                {id: 9,id_: 9 ,answer:'', text:'¿Cuáles de las siguientes cantidades es menor que 912?', image:'', 
                    posibleAnswers: [{id:'a', value: '200+10+2'},{id:'b', value: '10+10+10'},{id:'c', value: '5 centenas y 9 unidades'}]},
                {id: 10,id_: 10 ,answer:'', text:'De los siguientes objetos, ¿cuál tiene forma de cono? ', image:'/assets/images/quiz/level1/q10.png', 
                posibleAnswers: [{id:'a', value: 'a'},{id:'b', value: 'b'},{id:'c', value: 'c'}]}
              ];

              //console.log(self.data.group.registrations[0].id);

            /*if (self.data.group.registrations[0].registrationCountries.length === 0) {
                swal({
                    title: "!Importante!",
                    text: "En este momento no encontramos paises configurados para el rally.",
                    type: "error",
                    confirmButtonColor: "#AEEA00",
                    confirmButtonText: "Entendido",
                    closeOnConfirm: true
                }, function () {
                    window.location.href = '#/';
                });
            }*/
        }

        self.validateAnswers = function (answer, id) {
        
            var bandera=true;
            
            self.data.quiz.forEach(function(q) {
                if (q.id == id)
                    q.answer = answer.id;
                
                if (q.answer == "")
                    bandera=false;
                    

            });

            if (bandera)
                self.showSiguiente = true;
            
        }
        
       
        
        self.sendAnswers = function (){

            var registration = self.data.group.registrations[0].id;
            var quiz = self.data.quiz;
            var type = 1;

            //console.log(JSON.stringify({type: type, registration: registration, quiz:quiz}));

            $.ajax({
                type: "POST",
                url: "/rm-server-quiz/answers",
                contentType: "application/json",
                data: JSON.stringify({type: type, registration: registration, quiz:quiz}),
                dataType: "text", 
                success: function (data) { //alert(data);
                    
                    
                    window.location.href = "/auth/welcome.html#/quiz";
              
        
                },
                failure: function (errMsg) {
                    //alert(errMsg);
                }
            });

        }

    }

})();