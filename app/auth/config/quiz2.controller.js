(function () {
    'use strict';
    angular
        .module('app.auth')
        .controller('Quiz2Controller', Quiz2Controller);

    Quiz2Controller.$inject = ['PrivateFactory', 'InitializerFactory', 'PrivateService'];

    function Quiz2Controller(PrivateFactory, InitializerFactory, PrivateService) {
        var self = this;

        InitializerFactory.setInit(init);

        PrivateFactory.getDataGroup();

        function init() {
            self.data = PrivateFactory.data;
            
            self.showSiguiente = false;

            self.data.quiz =  [
                {id: 11, id_:1 ,answer:'', text:'¿Qué nombre recibe esta figura sólida? ', image:'/assets/images/quiz/level2/q1.png', 
                posibleAnswers: [{id:'a', value: 'Prisma'},{id:'b', value: 'Cono'},{id:'c', value: 'Cubo'}]},
                {id: 12, id_:2 ,answer:'', text:'La bandera de Perú está formada por 3 rectángulos. ¿Qué fracción muestra el color rojo de la bandera de Perú?', image:'/assets/images/quiz/level2/q2.png', 
                posibleAnswers: [{id:'a', value: '1/3'},{id:'b', value: '2/3'},{id:'c', value: '3/3'}]},
                {id: 13, id_:3 ,answer:'', text:'Se dice que, de cada 100 personas, 13 hablan quechua. ¿Cuál sería la mejor representación?', image:'', 
                posibleAnswers: [{id:'a', value: '13/100'},{id:'b', value: '100/13'},{id:'c', value: '13/10'}]},
                {id: 14, id_:4 ,answer:'', text:'¿Qué tipo de ángulo muestra la esquina de un marco de futbol?', image:'/assets/images/quiz/level2/q4.png', 
                posibleAnswers: [{id:'a', value: 'Agudo'},{id:'b', value: 'Obtuso'},{id:'c', value: 'Recto'}]},
                {id: 15, id_:5 ,answer:'', text:'El 50% es lo mismo que:', image:'', 
                posibleAnswers: [{id:'a', value: '50/100'},{id:'b', value: '25/100'},{id:'c', value: '75/100'}]},
                {id: 16, id_:6 ,answer:'', text:'¿Cuántos cubitos forman la siguiente figura?', image:'/assets/images/quiz/level2/q6.png', 
                posibleAnswers: [{id:'a', value: '8 unidades cúbicas'},{id:'b', value: '10 unidades cúbicas'},{id:'c', value: '6 unidades cúbicas'}]},
                {id: 17, id_:7 ,answer:'', text:'¿Cuál de las medidas es mayor a las demás?', image:'', 
                posibleAnswers: [{id:'a', value: '5 metros'},{id:'b', value: '500 centímetros'},{id:'c', value: '1 kilómetro'}]},
                {id: 18, id_:8 ,answer:'', text:'¿Qué sigue en este patrón 23, 33, 43…?', image:'', 
                    posibleAnswers: [{id:'a', value: '73'},{id:'b', value: '63'},{id:'c', value: '53'}]},
                {id: 19, id_:9 ,answer:'', text:'Si una Inka Cola en Perú cuesta 2.50 soles. ¿Cuánto pagarías por 2 Inka Colas?', image:'/assets/images/quiz/level2/q9.png', 
                    posibleAnswers: [{id:'a', value: '3 soles'},{id:'b', value: '4.50 soles'},{id:'c', value: '5 soles'}]},
                {id: 20, id_:10 ,answer:'', text:'Mira la tabla con la estatura de un grupo de niños. ¿Qué niño mide menos que los demás? ', image:'/assets/images/quiz/level2/q10.png', 
                posibleAnswers: [{id:'a', value: 'Luis mide menos'},{id:'b', value: 'María mide menos'},{id:'c', value: 'David mide menos'}]}
              ];

              //console.log(self.data.group.registrations[0].id);

            /*if (self.data.group.registrations[0].registrationCountries.length === 0) {
                swal({
                    title: "!Importante!",
                    text: "En este momento no encontramos paises configurados para el rally.",
                    type: "error",
                    confirmButtonColor: "#AEEA00",
                    confirmButtonText: "Entendido",
                    closeOnConfirm: true
                }, function () {
                    window.location.href = '#/';
                });
            }*/
        }

        self.validateAnswers = function (answer, id) {
            
                var bandera=true;
                
                self.data.quiz.forEach(function(q) {
                    if (q.id == id)
                        q.answer = answer.id;
                    
                    if (q.answer == "")
                        bandera=false;
                        
    
                });
    
                if (bandera)
                    self.showSiguiente = true;
                
            }
            
           
            
            self.sendAnswers = function (){
    
                var registration = self.data.group.registrations[0].id;
                var quiz = self.data.quiz;
                var type = 1;
    
                //console.log(JSON.stringify({type: type, registration: registration, quiz:quiz}));
    
                $.ajax({
                    type: "POST",
                    url: "/rm-server-quiz/answers",
                    contentType: "application/json",
                    data: JSON.stringify({type: type, registration: registration, quiz:quiz}),
                    dataType: "text", 
                    success: function (data) { //alert(data);
                        
                        
                        window.location.href = "/auth/welcome.html#/quiz";
                  
            
                    },
                    failure: function (errMsg) {
                        //alert(errMsg);
                    }
                });
    
            }
    
        }
    
    })();