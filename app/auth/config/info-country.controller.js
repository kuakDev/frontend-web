(function () {
    'use strict';

    angular
        .module('app.auth')
        .controller('InfoCountryController', InfoCountryController);

    InfoCountryController.$inject = ['$routeParams', 'PrivateFactory', 'PrivateService', 'InitializerFactory'];

    function InfoCountryController($routeParams, PrivateFactory, PrivateService, InitializerFactory) {
        var self = this;
        var selectedCountries = [];

        InitializerFactory.setInit(init);

        PrivateFactory.getDataGroup();
        

        function init() {
            self.data = PrivateFactory.data;

            if (self.data.group.registrations[0].registrationCountries.length > 0) {
                angular.forEach(self.data.group.registrations[0].registrationCountries, function (countries) {
                    if (countries.country.id === PrivateFactory.data.workingCountry) {
                        self.infoCountry = countries.country.informativeFile.description;
                    }
                });

            } else {
                swal({
                    title: "!Importante!",
                    text: "En este momento no encontramos la informacion del pais.",
                    type: "error",
                    confirmButtonColor: "#AEEA00",
                    confirmButtonText: "Entendido",
                    closeOnConfirm: true
                }, function () {
                    window.location.href = '#/';
                });
            }


            if (self.data.group.registrations[0].registrationCountries.length > 0) {
                angular.forEach(self.data.group.registrations[0].registrationCountries, function (countries) {
                    if (countries.country.id === PrivateFactory.data.workingCountry) {
                        angular.forEach(countries.country.resources, function (resource) {
                            if (resource.type === 'flag') {
                                self.flagCountry = resource.downloadUrl;
                            }
                        });
                    }
                });

            } else {
                swal({
                    title: "!Importante!",
                    text: "En este momento no encontramos el recurso del video.",
                    type: "error",
                    confirmButtonColor: "#AEEA00",
                    confirmButtonText: "Entendido",
                    closeOnConfirm: true
                }, function () {
                    window.location.href = '#map-countries';
                });
            }

        }


    }

})();