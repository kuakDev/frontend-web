(function () {
    'use strict';

    angular
        .module('app.auth')
        .controller('ConfigController', ConfigController);

    ConfigController.$inject = ['$routeParams', 'PrivateFactory', 'PrivateService', 'InitializerFactory'];

    function ConfigController($routeParams, PrivateFactory, PrivateService, InitializerFactory) {
        var self = this;
        var selectedCountries = [];

        InitializerFactory.setInit(init);

        PrivateFactory.getDataGroup();

        self.countries = [];

        function init() {
            self.data = PrivateFactory.data;

            if (self.data.rally && self.data.rally.rallyCountries.length > 0) {
                angular.forEach(self.data.rally.rallyCountries, function (countries) {
                    self.countries.push(countries.country);
                });

            } else {
                swal({
                    title: "!Importante!",
                    text: "En este momento no encontramos paises configurados para el rally.",
                    type: "error",
                    confirmButtonColor: "#AEEA00",
                    confirmButtonText: "Entendido",
                    closeOnConfirm: true
                }, function () {
                    window.location.href = '#/';
                });
            }

        }

        self.logout = function () {
            return PrivateService.logoff()
                .then(function () {
                    window.location.href = "/index.html";
                });
        };

        self.isSelected = function (country) {
            var i;
            for (i = 0; i < selectedCountries.length; i++) {
                if (selectedCountries[i] === country) {
                    return true;
                }
            }
            return false;
        }

        self.selectUnselect = function (country) {
            if (self.isSelected(country)) {
                var index = selectedCountries.indexOf(country);
                selectedCountries.splice(index, 1);
            } else {
                if (selectedCountries.length < 3) {
                    selectedCountries.push(country);
                } else {
                    swal({
                        title: "3 Paises.",
                        text: "Solamente puedes seleccionar 3 paises.",
                        type: "warning",
                        confirmButtonColor: "#AEEA00",
                        confirmButtonText: "Ok",
                        closeOnConfirm: true
                    }, function () {

                    });
                }
            }
        }

        self.register = function () {
            if (selectedCountries.length < 3) {
                swal({
                    title: "3 Paises.",
                    text: "Debes seleccionar 3 paises.",
                    type: "warning",
                    confirmButtonColor: "#AEEA00",
                    confirmButtonText: "Ok",
                    closeOnConfirm: true
                });
            } else {
                self.data.registration = PrivateService.registerCountries(selectedCountries, self.data.rally.id)
                    .then(function (response) {
                        window.location.href = '#map-countries';
                    });
            }
        }

    }

})();