(function () {
    'use strict';
    angular
        .module('app.auth')
        .controller('Quiz3Controller', Quiz3Controller);

        Quiz3Controller.$inject = ['PrivateFactory', 'InitializerFactory', 'PrivateService'];

    function Quiz3Controller(PrivateFactory, InitializerFactory, PrivateService) {
        var self = this;

        InitializerFactory.setInit(init);

        PrivateFactory.getDataGroup();

        function init() {
            self.data = PrivateFactory.data;
            
            self.showSiguiente = false;

            self.data.quiz =  [
                {id: 21, id_:1,answer:'', text:'¿Cómo se llama un ángulo de noventa grados?', image:'', 
                posibleAnswers: [{id:'a', value: 'Agudo'},{id:'b', value: 'Recto'},{id:'c', value: 'Obtuso'}]},
                {id: 22, id_:2,answer:'', text:'El 25% es lo mismo que:', image:'', 
                posibleAnswers: [{id:'a', value: '1/2'},{id:'b', value: '1/25'},{id:'c', value: '1/4'}]},
                {id: 23, id_:3,answer:'', text:'Es la relación entre radio y diámetro:', image:'', 
                posibleAnswers: [{id:'a', value: 'Diámetro es el doble del radio'},{id:'b', value: 'Diámetro es lo mismo que radio'},{id:'c', value: 'Diámetro es la mitad de radio'}]},
                {id: 24, id_:4,answer:'', text:'En un reloj, 20 minutos es lo mismo que:', image:'', 
                posibleAnswers: [{id:'a', value: '1/3 de hora'},{id:'b', value: '1/4 de hora'},{id:'c', value: '1/20 de hora'}]},
                {id: 25, id_:5,answer:'', text:'Es una figura sólida:', image:'', 
                posibleAnswers: [{id:'a', value: 'Rombo'},{id:'b', value: 'Esfera'},{id:'c', value: 'Rectángulo'}]},
                {id: 26, id_:6,answer:'', text:'¿Cuál de las medidas es mayor que las demás?', image:'', 
                posibleAnswers: [{id:'a', value: '8 cm'},{id:'b', value: '8 m'},{id:'c', value: '8 km'}]},
                {id: 27, id_:7,answer:'', text:'¿Cuál de las siguientes cantidades es menor que 203,400?', image:'', 
                posibleAnswers: [{id:'a', value: '203.400'},{id:'b', value: '20,340'},{id:'c', value: '203,400,000'}]},
                {id: 28, id_:8,answer:'', text:'¿Qué sigue en este patrón  150, 125, 100, . . .?', image:'', 
                    posibleAnswers: [{id:'a', value: '99'},{id:'b', value: '50'},{id:'c', value: '75'}]},
                {id: 29, id_:9,answer:'', text:'Si un galón de gasolina cuesta  R$ 10.25 en Brasil, ¿Cuánto cuestan 3 galones?', image:'', 
                    posibleAnswers: [{id:'a', value: 'R$  10.28'},{id:'b', value: 'R$  30.75'},{id:'c', value: 'R$  300.00'}]},
                {id: 30, id_:10,answer:'', text:'Mira la tabla con la estatura de un grupo de niños. ¿Qué niño es el más alto?', image:'/assets/images/quiz/level3/q10.png', 
                posibleAnswers: [{id:'a', value: 'Juan es el más alto'},{id:'b', value: 'María es la mas alta'},{id:'c', value: 'Carlos  es el más alto'}]}
              ];

             // console.log(self.data.group.registrations[0].id);

            /*if (self.data.group.registrations[0].registrationCountries.length === 0) {
                swal({
                    title: "!Importante!",
                    text: "En este momento no encontramos paises configurados para el rally.",
                    type: "error",
                    confirmButtonColor: "#AEEA00",
                    confirmButtonText: "Entendido",
                    closeOnConfirm: true
                }, function () {
                    window.location.href = '#/';
                });
            }*/
        }

        self.validateAnswers = function (answer, id) {
            
                var bandera=true;
                
                self.data.quiz.forEach(function(q) {
                    if (q.id == id)
                        q.answer = answer.id;
                    
                    if (q.answer == "")
                        bandera=false;
                        
    
                });
    
                if (bandera)
                    self.showSiguiente = true;
                
            }
            
           
            
            self.sendAnswers = function (){
    
                var registration = self.data.group.registrations[0].id;
                var quiz = self.data.quiz;
                var type = 1;
    
                //console.log(JSON.stringify({type: type, registration: registration, quiz:quiz}));
    
                $.ajax({
                    type: "POST",
                    url: "/rm-server-quiz/answers",
                    contentType: "application/json",
                    data: JSON.stringify({type: type, registration: registration, quiz:quiz}),
                    dataType: "text", 
                    success: function (data) { //alert(data);
                        
                        
                        window.location.href = "/auth/welcome.html#/quiz";
                  
            
                    },
                    failure: function (errMsg) {
                        //alert(errMsg);
                    }
                });
    
            }
    
        }
    
    })();