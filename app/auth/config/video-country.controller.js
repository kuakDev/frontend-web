(function () {
    'use strict';
    angular
        .module('app.auth')
        .controller('VideoCountryController', VideoCountryController);

    VideoCountryController.$inject = ['PrivateFactory', 'PrivateService', '$scope', 'InitializerFactory'];

    function VideoCountryController(PrivateFactory, PrivateService, $scope, InitializerFactory) {
        var self = this;

        InitializerFactory.setInit(init);

        PrivateFactory.getDataGroup();

        function init() {
            self.data = PrivateFactory.data;
            if (self.data.group.registrations[0].registrationCountries.length > 0) {
                angular.forEach(self.data.group.registrations[0].registrationCountries, function (countries) {
                    if (countries.country.id === PrivateFactory.data.workingCountry) {
                        angular.forEach(countries.country.resources, function (resource) {
                            if (resource.type === 'video') {
                                self.player = resource.downloadUrl;
                            }
                            if (resource.type === 'flag') {
                                self.flagCountry = resource.downloadUrl;
                            }
                        });
                    }
                });

            } else {
                swal({
                    title: "!Importante!",
                    text: "En este momento no encontramos el recurso del video.",
                    type: "error",
                    confirmButtonColor: "#AEEA00",
                    confirmButtonText: "Entendido",
                    closeOnConfirm: true
                }, function () {
                    window.location.href = '#map-countries';
                });
            }

        }

        self.playerVars = {
            controls: 1,
            autoplay: 1
        };

        self.jump = jump;


        $scope.$on('youtube.player.ended', function ($event, player) {
            window.location.href = '#info-country';
        });

        self.logout = function () {
            return PrivateService.logoff()
                .then(function () {
                    window.location.href = "/index.html";
                });
        };

        function jump() {
            window.location.href = '#info-country';
        }



    }

})();