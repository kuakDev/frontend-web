(function () {
    'use strict';
    angular
        .module('app.auth')
        .controller('MapCountriesController', MapCountriesController);

    MapCountriesController.$inject = ['PrivateFactory', 'InitializerFactory', 'PrivateService'];

    function MapCountriesController(PrivateFactory, InitializerFactory, PrivateService) {
        var self = this;

        InitializerFactory.setInit(init);

        PrivateFactory.getDataGroup();

        self.saveWorkingCountry = saveWorkingCountry;

        function saveWorkingCountry(id) {
            PrivateFactory.saveWorkingCountry(id);
        }

        function init() {
            self.data = PrivateFactory.data;
            if (self.data.group.registrations[0].registrationCountries.length === 0) {
                swal({
                    title: "!Importante!",
                    text: "En este momento no encontramos paises configurados para el rally.",
                    type: "error",
                    confirmButtonColor: "#AEEA00",
                    confirmButtonText: "Entendido",
                    closeOnConfirm: true
                }, function () {
                    window.location.href = '#/';
                });
            }
        }

    }

})();