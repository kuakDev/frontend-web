(function () {
    'use strict';

    angular
        .module('app.auth')
        .factory('PrivateFactory', PrivateFactory);

    PrivateFactory.$inject = ['PrivateService', 'InitializerFactory'];

    function PrivateFactory(PrivateService, InitializerFactory) {
        var data = {};

        data.confirTuto = false;
        var isPaused = false;


        return {
            getDataGroup: getDataGroup,
            data: data,
            saveWorkingCountry: saveWorkingCountry
        }

        function saveWorkingCountry(id) {
            data.workingCountry = id;
            return PrivateService.saveWorkingCountry(id)
                .then(function (result) {
                    return PrivateService.getMutipleValueQuestions(data.rally.id, id)
                        .then(function (result) {
                            return PrivateService.getStateMultipeQuestion(result[4].id)
                                .then(function (result) {
                                    console.log(result);
                                    if (result && result.questionAnswerState === "SUBMITTED") {
                                        window.location.href = '#game?rallyId=' + data.rally.id + '&countryId=' + id;
                                    } else {
                                        window.location.href = '#video-country';
                                    }
                                });

                        });

                });
        }

        function getDataGroup() {
            return PrivateService.getDataGroup()
                .then(function (result) {
                    if (result) {
                        data.group = result;
                        if (result.registrations && result.registrations.length > 0 && result.registrations[0].rally) {
                            if (result.registrations[0].rally.status === "PAUSED") {
                                if (!isPaused) {
                                    isPaused = true;
                                    window.location.href = "#complete";
                                }
                            } else {
                                /*if (!data.confirTuto) {
                                    //show something
                                }*/
                                data.rally = result.registrations[0].rally;
                                InitializerFactory.execInit();
                                return PrivateService.getRanking(data.rally.id)
                                    .then(function (result) {
                                        data.rankingList = result;
                                    });
                            }
                        } else {
                            return PrivateService.getActiveRally()
                                .then(function (result) {
                                    if (result && result.length > 0) {
                                        return PrivateService.register(result[0].id)
                                            .then(function (result) {

                                                console.log(result);

                                                if (result && result.rally.id) {
                                                    getDataGroup();
                                                    
                                                    if (result.rally.level.difficulty==1)
                                                        window.location.href = "#/quiz1";
                                                    else if (result.rally.level.difficulty==2)
                                                        window.location.href = "#/quiz2";
                                                    else if (result.rally.level.difficulty==3)
                                                        window.location.href = "#/quiz3";
            
                                                } else {
                                                    swal({
                                                        title: "!Ocurrio un error!",
                                                        text: "Ocurrió un error durante el registro, por favor contacta a cotacto@fucude.com",
                                                        type: "error",
                                                        confirmButtonColor: "#AEEA00",
                                                        confirmButtonText: "Entendido",
                                                        closeOnConfirm: true
                                                    });
                                                }
                                            });
                                    } else {
                                        swal({
                                            title: "!Importante!",
                                            text: "En este momento no encontramos rally en el que puedas participar.",
                                            type: "error",
                                            confirmButtonColor: "#AEEA00",
                                            confirmButtonText: "Entendido",
                                            closeOnConfirm: true
                                        });
                                    }
                                });
                        }
                    } else {
                        swal({
                            title: "No te encuentras autenticado.",
                            text: "Debes iniciar sesión.",
                            type: "warning",
                            confirmButtonColor: "#AEEA00",
                            confirmButtonText: "Continuar",
                            closeOnConfirm: true
                        }, function () {
                            window.location.href = "/index.html#/login";
                        });
                    }
                });
        }

    }

})();