(function () {
    'use strict';

    angular
        .module('app.auth')
        .factory('PrivateService', PrivateService);

    PrivateService.$inject = ['$http'];

    function PrivateService($http) {
        var SERVER = '/rm-server-web/rs/';
        var SERVER2 = '/api/v2/0/';


        return {
            getDataGroup: getDataGroup,
            getActiveRally: getActiveRally,
            getListCountries: getListCountries,
            registerCountries: registerCountries,
            logoff: logoff,
            getMutipleValueQuestions: getMutipleValueQuestions,
            register: register,
            saveWorkingCountry: saveWorkingCountry,
            getRanking: getRanking,
            saveScoreMultipleValueQuestion: saveScoreMultipleValueQuestion,
            getQuestionAnswerState: getQuestionAnswerState,
            getActiveAnswer: getActiveAnswer,
            getStateMultipeQuestion:getStateMultipeQuestion,
            saveInstitution: saveInstitution
        }

        function getStateMultipeQuestion(questionId) {
            return $http.get(SERVER + 'rallies/questions/' + questionId + '/answers/0/')
                .then(getResponseOk)
                .catch(getResponseFailed);

            function getResponseOk(response) {
                return response.data;
            }

            function getResponseFailed(error) {
                console.log('XHR Failed for service get state multiple value questions ');
            };
        }

        function getActiveAnswer() {
            return $http.get(SERVER + 'groups/0/answers/active')
                .then(getResponseOk)
                .catch(getResponseFailed);

            function getResponseOk(response) {
                return response.data;
            }

            function getResponseFailed(error) {
                console.log('XHR Failed for service get active answer');
            };
        }

        function getQuestionAnswerState(id) {
            return $http.get(SERVER + 'rallies/questions/' + id + '/answers')
                .then(getResponseOk)
                .catch(getResponseFailed);

            function getResponseOk(response) {
                return response.data;
            }

            function getResponseFailed(error) {
                console.log('XHR Failed for service get answer state ');
            };
        }


        function saveScoreMultipleValueQuestion(question, answer) {
            return $http.post(SERVER + 'rallies/questions/' + question + '/answers', answer)
                .then(getResponseOk)
                .catch(getResponseFailed);

            function getResponseOk(response) {
                return response.data;
            }

            function getResponseFailed(error) {
                console.log('XHR Failed for service save score ');
            };
        }

        function getRanking(rally) {
            return $http.get(SERVER + 'rallies/' + rally + '/ranking')
                .then(getResponseOk)
                .catch(getResponseFailed);

            function getResponseOk(response) {
                return response.data;
            }

            function getResponseFailed(error) {
                console.log('XHR Failed for service get raking ');
            };
        }

        function saveWorkingCountry(country) {
            return $http.post(SERVER + 'groups/0/countries/' + country + '/select')
                .then(getResponseOk)
                .catch(getResponseFailed);

            function getResponseOk(response) {
                return response.data;
            }

            function getResponseFailed(error) {
                console.log('XHR Failed for service register working countries in group ');
            };
        }



        function register(rallyId) {
            return $http.post(SERVER + 'rallies/' + rallyId + '/register')
                .then(getResponseOk)
                .catch(getResponseFailed);

            function getResponseOk(response) {
                return response.data;
            }

            function getResponseFailed(error) {
                console.log('XHR Failed for service register group in rally ');
            };
        }

        function getMutipleValueQuestions(rallyId, countryId) {
            return $http.get(SERVER + 'rallies/' + rallyId + '/countries/' + countryId + '/multivalue-question')
                .then(getResponseOk)
                .catch(getResponseFailed);

            function getResponseOk(response) {
                return response.data;
            }

            function getResponseFailed(error) {
                console.log('XHR Failed for get multiple value questions');
            }
        }

        function logoff() {
            return $http.get(SERVER + 'auth/logoff')
                .then(getResponseOk)
                .catch(getResponseFailed);

            function getResponseOk(response) {
                return response.data;
            }

            function getResponseFailed(error) {
                console.log('XHR Failed for logoff groups');
            }
        }

        function getListCountries() {
            return $http.get(SERVER + 'catalog/countries')
                .then(getResponseOk)
                .catch(getResponseFailed);

            function getResponseOk(response) {
                return response.data;
            }

            function getResponseFailed(error) {
                console.log('XHR Failed for service get Countries');
            }
        }

        function getActiveRally() {
            return $http.get(SERVER + 'rallies')
                .then(getResponseOk)
                .catch(getResponseFailed);

            function getResponseOk(response) {
                return response.data;
            }

            function getResponseFailed(error) {
                console.log('XHR Failed for service get rally active');
            };
        }

        function getDataGroup() {
            //return $http.get("group.json")
            return $http.get(SERVER + 'groups/0/')
                .then(getResponseOk)
                .catch(getResponseFailed);

            function getResponseOk(response) {
                return response.data;
            }

            function getResponseFailed(error) {
                console.log('XHR Failed for service get data gruop');
            };
        }


        function registerCountries(countries, rallyId) {
            var registerInfo = {};
            registerInfo.selectedCountries = countries;

            console.log(registerInfo);

            return $http.post(SERVER + 'rallies/' + rallyId + '/register/countries', registerInfo)
                .then(getResponseOk)
                .catch(getResponseFailed);

            function getResponseOk(response) {
                return response.data;
            }

            function getResponseFailed(error) {
                console.log('XHR Failed for service register countries in group ');
            };
        }

        function saveInstitution(institution) {
            var institutionData = {};
           institutionData.institution = institution;

            return $http.post(SERVER2 + 'institution', institutionData)
                .then(getResponseOk)
                .catch(getResponseFailed);

            function getResponseOk(response) {
                return response.data;
            }

            function getResponseFailed(error) {
                console.log('XHR Failed for service save score ');
            };
        }

    }


})();