(function () {
    'use strict';

    angular
        .module('app.auth')
        .controller('GameController', GameController);

    GameController.$inject = ['PrivateFactory', 'InitializerFactory'];

    function GameController(PrivateFactory, InitializerFactory) {
        var self = this;
        var selectedCountries = [];

        InitializerFactory.setInit(init);

        PrivateFactory.getDataGroup();

        self.countries = [];

        function init() {
            
            self.data = PrivateFactory.data;

            /**
             * Realiza el cambio de auto con conductor cuando sea requerido
             */

            if (self.data.group.transport.description.trim() != 'transport6' &&
                self.data.group.transport.description.trim() != 'transport7' &&
                self.data.group.transport.description.trim() != 'transport8' &&
                self.data.group.transport.description.trim() != 'transport9'){
                    self.data.group.transport.downloadUrl = "/assets/images/transports-avatars/"+self.data.group.avatar.description+"-"+self.data.group.transport.description+".png"
                }
            
        }
    }

})();