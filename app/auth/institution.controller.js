(function () {
    'use strict';
    angular
        .module('app.auth')
        .controller('InstitutionController', InstitutionController);

    InstitutionController.$inject = ['PrivateService'];

    function InstitutionController(PrivateService) {

        var self = this;

        self.saveInstitution = function(name){
        	  return PrivateService.saveInstitution(name)
                            .then(function(result){
                                //console.log('saved');
                            });

        }


    }

})();