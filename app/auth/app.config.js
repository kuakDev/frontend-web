(function() {
    'use strict';

    angular
        .module('app.auth', [
            'ngRoute',
            'youtube-embed',
            'ngSanitize',
            'ngTooltips'
        ])
        .config(configuration);

    configuration.$inject = ['$routeProvider'];

    function configuration($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'profile.html',
                controller: 'PrivateController',
                controllerAs: 'vm'
            })
            .when('/video-rally', {
                templateUrl: 'config/video-rally.html',
                controller: 'VideoRallyController',
                controllerAs: 'vm'
            })
            .when('/choose-countries', {
                templateUrl: 'config/choose-countries.html',
                controller: 'ConfigController',
                controllerAs: 'vm'
            })
            .when('/map-countries', {
                templateUrl: 'config/map-countries.html',
                controller: 'MapCountriesController',
                controllerAs: 'vm'
            })
            .when('/video-country', {
                templateUrl: 'config/video-country.html',
                controller: 'VideoCountryController',
                controllerAs: 'vm'
            })
            .when('/info-video-country', {
                templateUrl: 'config/gas.html',
                controller: 'GasController',
                controllerAs: 'vm'
            })
            .when('/info-country', {
                templateUrl: 'config/info-country.html',
                controller: 'InfoCountryController',
                controllerAs: 'vm'
            })
            
            .when('/ranking', {
                templateUrl: 'config/rank.html',
                controller: 'PrivateController',
                controllerAs: 'vm'
            })
            
            .when('/game', {
                templateUrl: 'game/game.html',
                controller: 'GameController',
                controllerAs: 'vm'
            })

            .when('/complete', {
                templateUrl: 'config/complete.html',
                controller: 'PrivateController',
                controllerAs: 'vm'
            })
            
            .when('/quiz', {
                templateUrl: 'config/quiz.html',
                controller: 'QuizController',
                controllerAs: 'vm'
            })

            .when('/quiz1', {
                templateUrl: 'config/quiz1.html',
                controller: 'Quiz1Controller',
                controllerAs: 'vm'
            })

            .when('/quiz2', {
                templateUrl: 'config/quiz2.html',
                controller: 'Quiz2Controller',
                controllerAs: 'vm'
            })

            .when('/quiz3', {
                templateUrl: 'config/quiz3.html',
                controller: 'Quiz3Controller',
                controllerAs: 'vm'
            })
            
            .when('/help', {
                templateUrl: 'help.html',
                controller: 'HelpController',
                controllerAs: 'vm'
            })

            .when('/institution', {
                templateUrl: 'institution.html',
                controller: 'InstitutionController',
                controllerAs: 'vm'
            })



            .when('/poll', {
                templateUrl: 'poll.html',
                controller: 'InstitutionController',
                controllerAs: 'vm'
            });   
            
        $routeProvider.otherwise({ redirectTo: '/' });
    }

})();