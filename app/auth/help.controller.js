(function () {
    'use strict';
    angular
        .module('app.auth')
        .controller('HelpController', HelpController);

    HelpController.$inject = ['PrivateService'];

    function HelpController(PrivateService) {

        var self = this;

        self.videoList = [];
        self.selectedStep = {};

        init();

        self.selectStep = selectStep;
        self.close = close;

        function close(video) {
            video.player.stopVideo();
            self.selectedStep = {};
        }

        function selectStep(step) {
            self.selectedStep = step;
        }

        function init() {
            return PrivateService.getActiveRally()
                .then(function (result) {
                    if (result && result.length > 0) {
                        self.videoList = result[0].resources;
                        angular.forEach(self.videoList, function (video, index) {
                            video.player = null;
                            video.vars = {
                                controls: 1,
                                autoplay: 0
                            };

                        });

                    }
                });
        }
    }

})();