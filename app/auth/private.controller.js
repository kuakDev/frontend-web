(function () {
    'use strict';

    angular
        .module('app.auth')
        .controller('PrivateController', PrivateController);

    PrivateController.$inject = ['PrivateFactory', 'PrivateService', 'InitializerFactory', '$filter'];

    function PrivateController(PrivateFactory, PrivateService, InitializerFactory, $filter) {
        var self = this;

        self.colors = ['div-warning', 'div-warning', 'div-info', 'div-danger', 'div-success'];

        InitializerFactory.setInit(init);

        PrivateFactory.getDataGroup();

        self.config = config;

        self.showGame = true;

        function init() {
    
            self.data = PrivateFactory.data;

            //console.log("habilita");
            self.showGame = true;

            //si no tiene institucion 
            var institutionName = self.data.group.institution.name;
            //console.log(institutionName);
            if (typeof institutionName == "undefined" || institutionName == null) {
                window.location.href = '#institution';
            }

        }


        self.hideButton = function () {
            
               //console.log("deshabilita");
                    self.showGame = false;
                //console.log("deshabilita");
                    
                
            }

        function config() {




            if (self.data && self.data.group && self.data.group.registrations[0]) {
                if (self.data.group.registrations[0].selectedCountry && self.data.group.registrations[0].selectedCountry.id) {
                    var countriesRegister = self.data.group.registrations[0].registrationCountries;
                    angular.forEach(countriesRegister, function (registerCountry) {
                        if (self.data.group.registrations[0].selectedCountry.id === registerCountry.country.id) {
                            if (registerCountry.state === 'COMPLETED') {
                                return PrivateService.getMutipleValueQuestions(self.data.rally.id, registerCountry.country.id)
                                    .then(function (result) {
                                        return PrivateService.getStateMultipeQuestion(result[4].id)
                                            .then(function (result) {
                                                if (result && result.questionAnswerState === "SUBMITTED") {
                                                    return PrivateService.getActiveAnswer()
                                                        .then(function (result) {
                                                            //console.log(result);

                                                            if (result && result.id) {
                                                                
                                                                window.location.href = '#game?rallyId=' + self.data.rally.id + '&countryId=' + registerCountry.country.id;
                                                                
                                                            } else {
                                                                var idRegistration = self.data.group.registrations[0].id;
                                                                var rallyLevel = self.data.group.registrations[0].rally.level.difficulty;
                                                                //window.location.href = '#map-countries';
                                                                //window.location.href = '#video-country';

                                                                var temp_ = self.data.group.registrations[0].registrationCountries;
                                                                var nextCountry = 0;

                                                                    
                                                                for (var i=0; i< temp_.length; i++){
                                                                    if (temp_[i].country.id > nextCountry && temp_[i].state === 'ACTIVE' ){
                                                                        nextCountry = temp_[i].country.id;
                                                                    }
                                                                }

                                                                if (nextCountry != 0){
                                                                    PrivateFactory.saveWorkingCountry(nextCountry);
                                                                    window.location.href = '#video-country';
                                                                }else{

                                                                    $.ajax({
                                                                        type: 'GET',
                                                                        url: '/rm-server/state-answers/'+idRegistration,
                                                                        success: function (data) {
    
                                                                         //   console.log(JSON.stringify(data));
                                                                            if (data.answers >= 12 && data.countQuiz <=2){
                                                                                if (rallyLevel==1)
                                                                                    window.location.href = "#/quiz1";
                                                                                else if (rallyLevel==2)
                                                                                    window.location.href = "#/quiz2";
                                                                                else if (rallyLevel==3)
                                                                                    window.location.href = "#/quiz3";
                                                                            }else{
                                                                                window.location.href = "#/complete";
                                                                            }
                                                                        }, 
                                                                        failure: function (errMsg1) {
                                                                        }
                                                                    });
                                                                   

                                                                }
                                                                
                                                            }


                                                        });
                                                } else {
                                                    PrivateFactory.saveWorkingCountry(self.data.group.registrations[0].selectedCountry.id);
                                                    window.location.href = '#video-country';
                                                }
                                            });
                                    });
                            } else {
                                //window.location.href = '#map-countries';
                                window.location.href = '#video-country';
                            }
                        }
                    });
                } else if (self.data.group.registrations[0].registrationCountries && self.data.group.registrations[0].registrationCountries.length > 0) {
                    //window.location.href = '#map-countries';

                    /*
                        Change flow 01/07/2017
                        Select the first active country;
                    */
                    
                    var temp_ = self.data.group.registrations[0].registrationCountries;
                    var nextCountry = 0;

                        
                    for (var i=0; i< temp_.length; i++){
                        if (temp_[i].country.id > nextCountry && temp_[i].state === 'ACTIVE' ){
                            nextCountry = temp_[i].country.id;
                        }
                    }

                    if (nextCountry != 0){
                        PrivateFactory.saveWorkingCountry(nextCountry);
                        window.location.href = '#video-country';
                    }

                } else {
                    //window.location.href = '#choose-countries';
                    /*
                        register countries
                        changes flow  01/07/2017 
                        select all the contries by default

                        register first country by default 
                    */

                    var selectedCountries = [];


                     if (self.data.rally && self.data.rally.rallyCountries.length > 0) {
                        var orderBy = $filter('orderBy');

                        //console.log(self.data.rally.rallyCountries);

                        for (var i = 0; i < self.data.rally.rallyCountries.length; i++) {
                            //console.log(self.data.rally.rallyCountries[i].id);
                            selectedCountries.push(self.data.rally.rallyCountries[i].country);
                        }

                        
                        if (selectedCountries.length) {

                            self.data.registration = PrivateService.registerCountries(selectedCountries, self.data.rally.id)
                                    .then(function (response) {
                                        //console.log(selectedCountries);
                                        //console.log(self.data.rally.id);
                                        //window.location.href = '#map-countries';


                                    var reload_ =   PrivateFactory.getDataGroup()
                                        .then (function (response_) {
                                        
                                        self.data = PrivateFactory.data;
                                        

                                        var temp_ = self.data.group.registrations[0].registrationCountries;

                                        var nextCountry = 0;

                                        for (var i=0; i< temp_.length; i++){
                                            
                                            if (temp_[i].country.id > nextCountry && temp_[i].state === 'ACTIVE' ){
                                                nextCountry = temp_[i].country.id;
                                            }
                                        }


                                        
                                        
                                        if (nextCountry != 0){
                                            PrivateFactory.saveWorkingCountry(nextCountry);
                                            window.location.href = '#video-country';
                                        }
                                                    

                            });

                            });

                        }else {
                            swal({
                                title: "!Importante!",
                                text: "Ocurrio un error en el registro #1001.",
                                type: "error",
                                confirmButtonColor: "#AEEA00",
                                confirmButtonText: "Entendido",
                                closeOnConfirm: true
                            }, function () {
                                window.location.href = '#/';
                            });
                        }

                    } else {
                        swal({
                            title: "!Importante!",
                            text: "En este momento no encontramos paises configurados para el rally.",
                            type: "error",
                            confirmButtonColor: "#AEEA00",
                            confirmButtonText: "Entendido",
                            closeOnConfirm: true
                        }, function () {
                            window.location.href = '#/';
                        });
                    }

                }
            } else {
                PrivateFactory.getDataGroup();
            }

        }

    }

})();