(function () {
    'use strict';

    angular
        .module('app.auth')
        .factory('InitializerFactory', InitializerFactory);

    InitializerFactory.$inject = [];

    function InitializerFactory() {
        var init;
        return {
            setInit: setInit,
            execInit: execInit
        }
        function execInit() {
            init();
        }

        function setInit(funct) {
            init = funct;
        }
    }

})();