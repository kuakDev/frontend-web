/// <reference path="../typings/jquery/jquery.d.ts" />
/*
    Choy 1.0.0
    2015, ioshmanu
    
    http://kuak.me
*/
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Choy;
(function (Choy) {
    var Panel = (function () {
        function Panel(posx, posy, posz, width, height) {
            this.posx = posx;
            this.posy = posy;
            this.posz = posz;
            this.width = width;
            this.height = height;
        }
        return Panel;
    })();
    var Question = (function () {
        function Question(text, inputType, posx, posy, posz, width, height) {
            //super(posx, posy, posz, width, height);
            this.text = text;
            this.inputType = inputType;
        }
        return Question;
    })();
    var Asset = (function (_super) {
        __extends(Asset, _super);
        function Asset(posx, posy, posz, width, height, animation, url) {
            _super.call(this, posx, posy, posz, width, height);
            this.animation = animation;
            this.downloadUrl = url;
        }
        return Asset;
    })(Panel);
    var Stage = (function () {
        function Stage(parentId, weather, children, question, idx) {
            this.parentId = parentId;
            this.weather = weather;
            this.children = children;
            this.question = question;
            this.idx = idx;
            this.next = idx + 1;
        }
        Stage.prototype.renderAssets = function () {
            var background = $('#' + this.parentId).addClass('background-animation');
            if (this.weather == 'day') {
                background.css("background-color", "#00BFFF");
            }
            else {
                if (this.weather == 'night') {
                    background.css("background-color", "#001848");
                }
            }
            this.children.forEach(function (child) {
                if(child.type !== 'answer'){
                    var panel = $('<div style="position:absolute; width:500px; height:200px; ' +
                        'left:' + child.posx + 'px; top:' + child.posy + 'px; z-index:' + child.posz + ';"></div>');
                    var el = $('<div class="' + child.animation + '"><h1>' +
                        '<img src="' + child.downloadUrl + '" height="' + child.height + '" width="' + child.width + '"></h1></div>');
                    panel.append(el);
                    background.append(panel);
                }
            });
        };
        Stage.prototype.renderQuestion = function () {
            var background = $('#' + this.parentId);
            var html = '<div class="questionbk"></div><div class="question">';
            var mimetype = '.jpeg,.jpg,.gif,.png,.pdf,.doc,.docx,.ppt,.pptx';
            html += '<div class="row">';
            html += '<div class="col-xs-3 col-sm-3 col-md-4">';
            html += '   <div class="coin gold"><p>' + this.next + '</p> </div>';
            html += '</div>';
            html += '<div class="col-xs-9 col-sm-9 col-md-8">';
            html += '   <h3>'+this.question.name+'</h3>';
            html += '</div>';
            html += '</div>';
            html += this.question.text + '<br/>';
            var i;
            if (this.question.inputType == "input") {
                html += '<input type="input" />';
            }
            else {
                if (this.question.inputType == "file") {
                    html += '<div class="alert alert-info" role="alert">'+
                        'Formatos de documento/images válidos: pdf, ppt, pptx, jpg, png, gif'+
                        '</div>';
                    html += '<div class="alert alert-danger" role="alert">'+
                            'Recuerda subir el archivo que corresponda al reto de la semana'+
                            '</div>';
                    html += '<input style="background-color: #03A9F4 !important; font-size: 16px;" class="form-control" accept="'+mimetype +'" id="fileupload' + this.question.questionId + '" type="file" name="file" multiple alt="' + this.next + '">';
                    html += '<div id="progress' + this.question.questionId + '" class="progress"><div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="min-width: 2em;"></div></div>';
                }
            }
            //q.append(i);

            html += '<div class="text-center h1" id="answers' + this.question.questionId + '"></div>'
            html += "<hr/>"
            html += '<button onclick="changeStage(' + this.next + ');" class="btn btn-primary btn-block animated infinite pulse" id="next' + this.question.questionId + '">Continuar</button>';

            //q.append(b);
            html += '</div>';
            var d = $('<div></div>');
            d.html(html);
            var question = this.question;
            background.append(d).each(function () {
                //console.log("creating file uploader");
                createFileUpload(question);
            });
        };
        
        Stage.prototype.renderAnswers = function(){
            
        };
        
        Stage.prototype.render = function (next) {
            this.renderAssets();
            this.renderQuestion();
            this.renderAnswers();
        };
        return Stage;
    })();
    Choy.Stage = Stage;
})(Choy || (Choy = {}));
//Create a new instance of a stage
//var st = new Choy.Stage("cuerpo", "abc", resources);
//st.render();  
