/// <reference path="../typings/jquery/jquery.d.ts" />
/*
    Choy 1.0.0
    2015, iojosemanu
    
    http://kuak.me
*/

module Choy {
    class Panel {
        posx: number;
        posy: number;
        posz: number;
        width: number;
        height: number;
        
        constructor(posx: number, posy: number, posz: number, width: number, height: number) {
            this.posx = posx;
            this.posy = posy;
            this.posz = posz;
            this.width = width;
            this.height = height;
        }
    }
    
    class Question{
        text: string;
        inputType: string;
        
        constructor(text: string, inputType: string, posx: number, posy: number, posz: number, width: number, height: number) {
            //super(posx, posy, posz, width, height);
            this.text = text;
            this.inputType = inputType;
        }
    }
    
    class Asset extends Panel{
        animation: string;
        downloadUrl: string;
        
        constructor(posx: number, posy: number, posz: number, width: number, height: number, animation: string, url: string) {
            super(posx, posy, posz, width, height);
            this.animation = animation;
            this.downloadUrl = url;
        }
    }
    
    export class Stage {
        parentId: string;
        weather: string;
        children: Array<Asset>;
        question: Question;
        idx: number;
        next: number;
        
        constructor(parentId: string, weather: string, children: Array<Asset>, question: Question, idx: number) {
            this.parentId = parentId;
            this.weather = weather;
            this.children = children;
            this.question = question;
            this.idx = idx;
            this.next = idx + 1;
        }
        
        renderAssets() {
            var background = $('#' + this.parentId).addClass('background-animation');
            if(this.weather == 'day') {
                background.css("background-color","#00BFFF");
            }else {
                if(this.weather == 'night') {
                    background.css("background-color","#001848");
                }
            }
            this.children.forEach(child => {
                                
                var panel = $('<div style="position:absolute; width:500px; height:200px; ' +
					'left:' + child.posx + 'px; top:' + child.posy + 'px; z-index:' + child.posz + ';"></div>');
                    
				var el = $('<div class="' + child.animation + '"><h1>' +
                            '<img src="' + child.downloadUrl + '" height="' + child.height + '" width="' + child.width + '"></h1></div>');
                            
				panel.append(el);
				background.append(panel);
            });
        }
        
        renderQuestion() {
            var background = $('#' + this.parentId);
            var html = '<div class="questionbk"></div><div class="question">';
            html += '<div class="coin gold"><p>'+ this.next +'</p></div>'; 
            html += this.question.text + '<br/>';
            var i;
            if(this.question.inputType == "input") {
                html += '<input type="input" />';
            }else {
                if(this.question.inputType == "file") {
                    html += '<input type="file" />';
                }
            }
            //q.append(i);
            html += ' <button onclick="changeStage(' + this.next + ');" class="btn btn-primary">Continuar</button>';
            //q.append(b);
            html += '</div>';
            var d = $('<div></div>');
            d.html(html);
            background.append(d);
        }
        
        render(next) {
            this.renderAssets();
            this.renderQuestion();
        }
         
    }

}


//Create a new instance of a stage
//var st = new Choy.Stage("cuerpo", "abc", resources);
//st.render();