var stages = [];
$(document).ready(function () {
    var params = getUrlVars();
	
    $.get("/rm-server-web/rs/groups/0/answers/active", function (result) {
        var stagesInfo = [result.question];
        //create first stage
        var idx = 0;
        $.each(stagesInfo, function (k, stage) {
            stages.push(new Choy.Stage("background" + idx, stage.background, stage.resources, { text: stage.plainText, inputType: stage.inputType , questionId: stage.id, name: stage.name}, idx));
            idx++;
        });

        stages[0].render();
    });
});

function changeStage(idx) {

    console.log(stages);
    
    var question = stages[idx - 1].question;
    
    var data = {
        id : question.questionId
    };

    var idQ = data.id;

    var answ = $('input:input').val();
    $.ajax({
        type: "POST",
        url: "/rm-server-web/rs/rallies/questions/" + question.questionId + "/answers",
        contentType: "application/json",
        data: JSON.stringify({answer: answ, resources:question.uploadInfo.result.files}),
        dataType: "json",
        xhrFields: {
      withCredentials: true
   }, 
        success: function (data) { //alert(data);
            $.ajax({
                type: 'GET',
                url: '/rm-server/update-answers/'+idQ,
                success: function (data) {
                    //console.log(idx+idx);
                    //console.log(stages.length);
                    if(idx + 1 > stages.length){
                        window.location.href = "/auth/welcome.html#/";
                        return;
                    }
                }, 
                failure: function (errMsg1) {
                }
            })
            
        },
        failure: function (errMsg) {
            //alert(errMsg);
        }
    });


    $('#background' + (idx - 1)).addClass('background-close');
    setTimeout(function () {
        $('#background' + (idx - 1)).html("");
    }, 5000);
    stages[idx].render();
}

function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars; 
}
function createFileUpload(question){
    $("#next"+question.questionId).hide();
    $("#progress"+question.questionId).hide();
    $("#fileupload"+question.questionId).fileupload({
        add: function(e, data) {
            var uploadErrors = [];
            var acceptFileTypes = /(\.|\/)(gif|jpe?g|png|pdf)$/i
            if(data.originalFiles[0]['type'].length && (!acceptFileTypes.test(data.originalFiles[0]['type'])
                || data.originalFiles[0]['type'] == "application/vnd.ms-powerpoint" 
                || data.originalFiles[0]['type'] == "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                || data.originalFiles[0]['type'] == "application/msword" 
                || data.originalFiles[0]['type'] == "application/vnd.openxmlformats-officedocument.presentationml.presentation")) {
                console.log(data.originalFiles[0]['type']);
                uploadErrors.push('Formato de archivo no aceptado');
                console.log("Formato no esperado");
                sweetAlert(":(", "Formato no soportado", "error");
            }
            if(data.originalFiles[0]['size'].length && data.originalFiles[0]['size'] > 10000000) {
                uploadErrors.push('Archivo demasiado grande');
                console.log("archivo grande");
                sweetAlert(":(", "El archivo es demasiado grande", "error");
            }
            if(uploadErrors.length > 0) {
                //alert(uploadErrors.join("\n"));
            } else {
                data.submit();
            }
        },
        url: '/rm-server-web/rs/files/answer/',
        dataType: 'json',
        //acceptFileTypes: /(\.|\/)(docx?|pdf|pptx?)$/i,
        done: function (e, data) {
            $.each(data.result.files, function (index, file) {
                $('<a href="' + file.downloadUrl + '" target="_blank"><span class="glyphicon glyphicon-download-alt"/></a>').appendTo($("#answers" + question.questionId));
            });
            $("#progress" + question.questionId).hide();
            $("#next"+question.questionId).show();
            question.uploadInfo = data;

            console.log("file del registration"+file.id);
        },
        progressall: function (e, data) {
            console.log("Progreso: " + data.loaded)
            $("#fileupload" + question.questionId).hide();
            $("#progress" + question.questionId).show();
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress' + question.questionId + ' .bar').css(
                'width',
                progress + '%'
            );
        },
        fail: function(e, data){
            sweetAlert(":(", "Ocurrio un error inesperado", "error");
        }

    });
};