var btnSound = new buzz.sound("/assets/music/click", {
    formats: ["mp3"]
});

var hoverSound = new buzz.sound("/assets/music/hover", {
    formats: ["mp3"]
});



function logout() {
    swal({
        title: "¿Estas seguro?",
        text: "¿Deseas salir del juego?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "SI",
        cancelButtonText: "NO",
        closeOnConfirm: false
    }, function () {
        // swal("Cerrar Sesion", "Has salido del juego", "success"); 
        window.location.href = "index.html";
    });

};


$(document).ready(function () {
    setTimeout(function () {
        $('body').addClass('loaded');
        $('h1').css('color', '#222222');

        $('.btn').on("mouseover", function (e) {
            hoverSound.play();
        });

        $('.btn').on('click', function () {
            btnSound.play();
        });


    }, 3000);



});