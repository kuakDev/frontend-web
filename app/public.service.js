(function () {
    'use strict';

    angular
        .module('app')
        .factory('PublicService', PublicService);

    PublicService.$inject = ['$http'];

    function PublicService($http) {
        
        var SERVER = '/rm-server-web/rs/';

        return {
            getActiveRally: getActiveRally
        }

        function getActiveRally() {
            return $http.get(SERVER + 'rallies')
                .then(getResponseOk)
                .catch(getResponseFailed);

            function getResponseOk(response) {
                return response.data;
            }

            function getResponseFailed(error) {
                console.log('XHR Failed for service get rally active');
            };
        }
    }

})();