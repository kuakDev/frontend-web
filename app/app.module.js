(function () {
    'use strict';

    angular
        .module('app', [
            'ngRoute',
            'youtube-embed',
            'ngTooltips'
        ])
        .config(configuration);

    configuration.$inject = ['$routeProvider'];

    function configuration($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'public.html'
                //templateUrl: 'preview.html'
            })
            .when('/preview', {
                templateUrl: 'preview.html'
            })
            .when('/login', {
                templateUrl: 'login.html'
                //templateUrl: 'preview.html'
            })
            .when('/signup', {
                templateUrl: 'group/signup.html',
                controller: 'ConfigController',
                controllerAs: 'vm'
            })

            .when('/avatar', {
                templateUrl: 'group/avatar.html',
                controller: 'AvatarController',
                controllerAs: 'vm'
            })
            .when('/transport', {
                templateUrl: 'group/transport.html',
                controller: 'TransportController',
                controllerAs: 'vm'
            })
            .when('/help', {
                templateUrl: 'help.html',
                controller: 'HelpController',
                controllerAs: 'vm'
            });
        $routeProvider.otherwise({ redirectTo: '/' });
    }

})();